# Databricks notebook source
# MAGIC %md
# MAGIC ### Notebook: search_pdf_reports
# MAGIC
# MAGIC As part of the inside the scraping_emissions_pipeline, 
# MAGIC this notebook is responsible for finding URLs of .pdf files, that stand for companies' CDP Climate Questionaire Disclosure.
# MAGIC
# MAGIC The steps of this notebook are as follows:
# MAGIC
# MAGIC 1. google company names with certain well-chosen keywords
# MAGIC 2. filter the google search results URLs, if they comply with usual name string of such pdf URLs
# MAGIC 3. collect URLs into a table

# COMMAND ----------

# MAGIC %md
# MAGIC ##### Create Database for repo, and set it for use

# COMMAND ----------

spark.sql(f"CREATE SCHEMA IF NOT EXISTS scraping_emissions_database")
spark.sql(f"USE SCHEMA scraping_emissions_database")

# COMMAND ----------

# MAGIC %md
# MAGIC ###### Load company names from input csv

# COMMAND ----------

import csv
company_names = []
with open('input/constituents - MSCI World.csv', 'r') as file:
    reader = csv.reader(file)
    for r, row in enumerate(reader):
        if r == 0:
            continue
        company_names.append(row[0])

# COMMAND ----------

# MAGIC %md
# MAGIC ###### Defining a set of keywords to search for to find emission report pdf URLS.

# COMMAND ----------

keywords = [
    'CDP',
    'climate',
    'change',
    'response',
    'questionnaire',
    'PDF'
]

# COMMAND ----------

def eval_url(candidate_url, collected_urls):
    """
    This function investigates an url string.
    Returns True if it seemingly stands for a CDP climate response .pdf file, and if it has not been collected yet.
    Returns False otherwise
    """

    if \
        (candidate_url in collected_urls) | \
        ("cdp-disclosure-platform-guide" in candidate_url.lower()) | \
        ("//cdn.cdp.net"  in candidate_url.lower()) | \
        ("//guidance.cdp.net" in candidate_url.lower()) | \
        (".pdf" not in candidate_url.lower()):
            
        return False
    
    candidate_url_ending = candidate_url.split('/')[-1]
    candidate_url_ending = candidate_url_ending.lower().replace("%20", "-")

    if \
        (('cdp' in candidate_url_ending) & ('climate' in candidate_url_ending)) | \
        (('cdp' in candidate_url_ending) & ('response' in candidate_url_ending)) | \
        (('cdp' in candidate_url_ending) & ('questionnaire' in candidate_url_ending)) | \
        (('climate' in candidate_url_ending) & ('change' in candidate_url_ending)) | \
        (('climate' in candidate_url_ending) & ('response' in candidate_url_ending)) | \
        (('climate' in candidate_url_ending) & ('questionnaire' in candidate_url_ending)):
            
        return True
    
    else:
        return False

# COMMAND ----------

# MAGIC %md
# MAGIC ###### Main part: search, filter, log

# COMMAND ----------

from googlesearch import search
from pyspark.sql.types import StructType, StructField, StringType, IntegerType
from pyspark.sql.functions import col
import time


df_schema = StructType([
    StructField('pkey', IntegerType(), nullable=False), 
    StructField('company_report_url', StringType(), nullable=False),
    StructField('company_searched', StringType(), nullable=False),
    StructField('search_term', StringType(), nullable=False)
])
data_df_all = spark.createDataFrame(data=[], schema=df_schema)
search_urls_existing = []

counter = data_df_all.count()
for company_name in company_names[:100]:

    search_term = ' '.join([company_name] + keywords)
    # Google-search.search returns multiple url results. We limit that to 10.
    for res_url in search(search_term, tld="co.in", num=10, stop=10, pause=2):

        keep_url = eval_url(res_url, search_urls_existing)
        if keep_url:
            
            data_item = {
                "pkey": counter,
                "company_report_url": res_url,
                "company_searched": company_name,
                "search_term": ' '.join(keywords)
            }
            data_df = spark.createDataFrame(data=[data_item], schema=df_schema)
            data_df_all = data_df_all.union(data_df)
            search_urls_existing.append(res_url)

            counter += 1
        # time.sleep(3)  # google may detect automated crawling


data_df_all = data_df_all.dropDuplicates(['company_report_url'])
data_df_all = data_df_all.sort("pkey")

# COMMAND ----------

# Initialize table for company emission report pdf urls
table_name = 'company_emission_report_urls'
table_schema = "pkey INT, company_report_url STRING, company_searched STRING, search_term STRING"
spark.sql(
    f"""
        CREATE TABLE IF NOT EXISTS {table_name}
        ({table_schema})
        TBLPROPERTIES ("scraping_emissions_pipeline.quality" = "bronze")
    """
)

data_df_all.write.insertInto(table_name)

# COMMAND ----------

# MAGIC %md
# MAGIC ###### Display collected URLs for visual inspection

# COMMAND ----------

table_name = 'company_emission_report_urls'
data_df_all_read = spark.table(f"hive_metastore.scraping_emissions_database.{table_name}")
display(data_df_all_read)
