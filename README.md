# webscraping_company_emissions

This pipeline aims to scrape the internet for companies' self-reported emissions (scope 1, 2, 3).
This is a realistic task, and shall be investigated how effective the data collection works, as most companies respond to a so called Climate Change Questionary of CDP (non-profit organisation), anually. Companies voluntarily have the option to publish their answers, which they upload usually to their website, in .pdf format.

These pdf files follow more or less the same format, and once parsed to text data, that text data can be ran through an NLP model to extraxct the desired emission data points.