# Databricks notebook source
# MAGIC %md
# MAGIC ### Notebook: nlp_text_to_get_json_data
# MAGIC
# MAGIC As part of the scraping_emisssions_pipeline,
# MAGIC this notebook is responsible for "extracting" the actual emission datapoints from filtrated text reports.
# MAGIC This is achieved by sending the prior cleaned pdf text as content with certain prompts to the OpenAI's API.
# MAGIC The API, based on their pre-trained GPT 3.5 turbo model, generates an output as specified in the prompt (json).
# MAGIC
# MAGIC Seemingly very successfully.
# MAGIC
# MAGIC Methodology explained:
# MAGIC
# MAGIC As text is too long, OpenAI's API may refuse our request. Therefore text is split up into chunks. However, the API can deal with additional requests following the initial while still remembering the previously loaded content, and intitial response. Exploiting this characteristic of the API, when sending second, third, ... chunks of the text, a much shorter prompt is enough.

# COMMAND ----------

# MAGIC %md
# MAGIC ##### Create Database for repo, and set it for use

# COMMAND ----------

spark.sql(f"CREATE SCHEMA IF NOT EXISTS scraping_emissions_database")
spark.sql(f"USE SCHEMA scraping_emissions_database")

# COMMAND ----------

# MAGIC %md
# MAGIC ###### Load text silver data

# COMMAND ----------

df_text = spark.table("hive_metastore.scraping_emissions_database.company_emission_report_text")
df_text = df_text.sort('pkey')
display(df_text)

# COMMAND ----------

# MAGIC %md
# MAGIC ###### Defining functions

# COMMAND ----------

from io import StringIO
from nltk.tokenize import sent_tokenize
import nltk
nltk.download('punkt')

def split_text(text, chunk_size=7000):
    """
    Splits the given text into chunks of approximately the specified chunk size.

    Args:
    text (str): The text to split.

    chunk_size (int): The desired size of each chunk (in characters).

    Returns:
    List[str]: A list of chunks, each of approximately the specified chunk size.
    """

    chunks = []
    current_chunk = StringIO()
    current_size = 0
    sentences = sent_tokenize(text)
    for sentence in sentences:
        sentence_size = len(sentence)
        if sentence_size > chunk_size:
            while sentence_size > chunk_size:
                chunk = sentence[:chunk_size]
                chunks.append(chunk)
                sentence = sentence[chunk_size:]
                sentence_size -= chunk_size
                current_chunk = StringIO()
                current_size = 0
        if current_size + sentence_size < chunk_size:
            current_chunk.write(sentence)
            current_size += sentence_size
        else:
            chunks.append(current_chunk.getvalue())
            current_chunk = StringIO()
            current_chunk.write(sentence)
            current_size = sentence_size
    if current_chunk:
        chunks.append(current_chunk.getvalue())
    return chunks

def extend_prompt_with_content(prompt, t_chunks):
    """
    """
    prompt_extended = []
    for i, t_chunk in enumerate(t_chunks):
        if i == 0:
            prompt_extended.append(
                '\n'.join([prompt[0], t_chunk])
            )
        else:
            prompt_extended.append(
                '\n'.join([prompt[1], t_chunk])
            )
    return prompt_extended

# COMMAND ----------

import openai
import json
def extraxt_data_with_openai(prompt_, text_):
    """
    Firing up the chat like dialog of OpenAI API
    """

    text_chunks = split_text(text_, 7000)
    prompt_with_content = extend_prompt_with_content(prompt_, text_chunks)

    with open('keys/openai_api_key.txt') as f:
        my_api_key = f.readlines()
        openai.api_key = my_api_key[0]

    messages = [{'role': 'system', 'content': 'You are a helpful assistant.'}]
    for i, prompt_i in enumerate(prompt_with_content):
        # Add prompt as user message to messages
        messages.append({'role': 'user', 'content': prompt_i})

        # Send the API request
        response = openai.ChatCompletion.create(
            model="gpt-3.5-turbo", messages=messages  # temperature=0.7, max_tokens=1000
        )

        response_content = str(response.choices[0].message['content'])
        # Add response as assistant's message to messages
        messages.append({"role": "assistant", "content": response_content})
        break

    # Extract data from the response
    response_final = json.loads(response_content)
    return response_final

# COMMAND ----------

# MAGIC %md
# MAGIC ###### Main part: declare prompt, loop over texts, slit them to chunks, send them to OpenAI, log response json to silver table.

# COMMAND ----------

prompt_init = """
    Want to extract data in JSON structure as follows:
    
    {
        "Document Name": "..",
        "Identifier: {
            "Company Name": "..",
            "ISIN": "..",
            "Sedol": "..",
            "Cusip": "..",
            "Ticker": "..",
        },
        "Metric": "..",
        "Emissions": [
            {
                "Start Date": "..",
                "End Date": "..",
                "Scope 1": "..",
                "Scope 2 location-based": "..",
                "Scope 2 market-based": "..",
            }
        ]
    }

    The fields can be fuzzy, so do not be strict on commas, brackets, dashes, leading, and trailing strings.
    Return Date fields as String in "YYYY-MM-DD" format.
    Return Score 1, Scope 2 in Float format.
    Return all other fields in String format.
    Return only the json, without explanations.
    If you see "metric tons CO2e" in the text, "Metric" field can be filled in with that.
    If a field cannot be filled, make it null.
    If Emissions are available for multiple dates, append them to the list.

    The content is as follows:
"""

prompt_next = f"""
    Update, and or extend the json output if further data can be extracted from this content:
"""

# COMMAND ----------


prompt = [prompt_init, prompt_next]
data_all = []
for df_row in df_text.collect():
    pkey = df_row['pkey']
    text = df_row['company_report_text']

    data_item = extraxt_data_with_openai(prompt, text)
    data_all.append(data_item)
    break

# COMMAND ----------

data_item
