# Databricks notebook source
# MAGIC %md
# MAGIC ### Notebook: extract_transform_pdf_urls
# MAGIC
# MAGIC As part of the scraping_emissions_pipeline, this notebook is responsible for 
# MAGIC
# MAGIC 1. making URL requests for the prior collected URLs
# MAGIC 2. parsing those .pdf data behind the URLs
# MAGIC 3. transforming the pdf data to text data
# MAGIC 4. logging that text data to a new table layer

# COMMAND ----------

# MAGIC %md
# MAGIC ##### Create Database for repo, and set it for use

# COMMAND ----------

spark.sql(f"CREATE SCHEMA IF NOT EXISTS scraping_emissions_database")
spark.sql(f"USE SCHEMA scraping_emissions_database")

# COMMAND ----------

# MAGIC %md
# MAGIC ###### Load URLs table

# COMMAND ----------

df_urls = spark.table("hive_metastore.scraping_emissions_database.company_emission_report_urls").select("pkey", "company_report_url")
df_urls = df_urls.sort('pkey')
display(df_urls)

# COMMAND ----------

# MAGIC %md
# MAGIC ###### Defining functions to clean text data 

# COMMAND ----------

import re
import copy
from io import StringIO


def remove_page_count(t):
    pattern = r"CDP\nPage\n(\d{3}|\d{2}|d{1})\nof (\d{3}|\d{2}|d{1})"
    t = re.sub(pattern, r'', t)

    pattern = r"\n\n"
    t = re.sub(pattern, r'', t)

    return t


def clean_text_records(t_inp):
    """
    Cleans text from frequently occurring substring errors.

    :param t_inp: string
    :return:    string
    """

    t_out = copy.deepcopy(t_inp)

    # Remove multiple whitespaces, linebreaks
    t_out = re.sub(" (\n)", "\n", t_out)
    t_out = re.sub("(\n)+", "\n", t_out)

    # Swap dates %MMM %d %YYYY divided by newline to be divided by comma, whitespace
    pattern = r"((?<=[1-9])|(?<=[0-2][0-9])|(?<=[30|31])),\n(?=(19|20[0-9][0-9]))"
    t_out = re.sub(pattern, r', ', t_out)

    # Swap non-capitalized words lead by newline to be lead by whitespace
    pattern = r"\b(-?)(,?)\n(?=[a-z]+)"
    t_out = re.sub(pattern, r" ", t_out)

    # Truncate text paragraphs to 15 word counts
    t_lines = t_out.split('\n')
    t_lines_trunc = []
    for tl in t_lines:
        tl_trunc = ' '.join(tl.split(' ')[:15])
        t_lines_trunc.append(tl_trunc)
    t_out = '\n'.join(t_lines_trunc)

    t_out = t_out.replace('<Not Applicable>', 'N/A')
    t_out = t_out.replace('<Not\nApplicable>', 'N/A')

    return t_out


def is_row_of_new_section(line, current_section):
    """

    :param line:            String
    :param current_section: String
    :return:                Boolean
    """
    line_str = str(line)
    if line_str.startswith('C') and line_str[1].isdigit():
        if '.' in line_str:
            section_recognized = line_str.split('.')[0]
            current_section_num = current_section.split('.')[0]
            if current_section_num == section_recognized:
                return False
        return True
    return False


def filter_text_to_sections_selected(txt, sections):
    """

    :param txt:         string.
    :param sections:    list of strings, filter keeps any section that contains any element of this list
    :return:            string
    """
    txt_split = txt.split('\n')
    section_of_t = 'Title.'
    text_per_sect = {section_of_t: []}
    for t in txt_split:
        if is_row_of_new_section(t, section_of_t):
            section_of_t = t
            text_per_sect[section_of_t] = []
        else:
            text_per_sect[section_of_t].append(t)

    txt_per_sect_keep = {
        key: val
        for key, val in text_per_sect.items()
        if any([sect in key for sect in sections])
    }
    txt_melt = [v for val in txt_per_sect_keep.values() for v in val]
    txt_out = '\n'.join(txt_melt)
    return txt_out
    """

    :param txt:         list or Series of strings.
    :param sections:    list of strings, filter keeps any section that contains any element of this list
    :return:            list or Series of strings filtered to selected text sections
    """
    section_of_t = 'Title.'
    text_per_sect = {section_of_t: []}
    for t in txt:
        if is_row_of_new_section(t, section_of_t):
            section_of_t = t
            text_per_sect[section_of_t] = []
        else:
            text_per_sect[section_of_t].append(t)

    text_per_sect_keep = {
        key: val
        for key, val in text_per_sect.items()
        if any([sect in key for sect in sections])
    }
    text_melt = [v for val in text_per_sect_keep.values() for v in val]
    return text_melt


# COMMAND ----------

# MAGIC %md
# MAGIC ###### Defining pdf parser function, also includes the URL request itself. PDF files are not downloaded and stored, only their URLs

# COMMAND ----------

from io import BytesIO
import requests
from pyspark.sql.types import StringType
from pyspark.sql.functions import col, udf

from pdfminer.high_level import extract_text  # does not work as it should with tables
from pdfminer.layout import LAParams, LTTextBox
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import PDFPageAggregator
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter

def parse_pdf(verb, url):
    headers = {
        'User-Agent': (
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5)'
            'AppleWebKit/537.36 (KHTML, like Gecko)'
            'Chrome/45.0.2454.101 Safari/537.36'
        )
    }
    # Make API request, get response object back, create dataframe from above schema.
    try:
        if verb == "get":
            res = requests.get(url, headers=headers, timeout=2)
        else:
            res = requests.post(url, headers=headers, timeout=2)
    except Exception as e:
        return None

    if res != None and res.status_code == 200:
        fp = BytesIO(res.content)
        try:
            pdf_resource_manager = PDFResourceManager()
            laparams = LAParams()
            device = PDFPageAggregator(pdf_resource_manager, laparams=laparams)
            interpreter = PDFPageInterpreter(pdf_resource_manager, device)

            pages = PDFPage.get_pages(fp)

            y_max = 0
            pdf_xloc = []
            pdf_yloc = []
            pdf_text = []
            for page_i, page in enumerate(pages):
                interpreter.process_page(page)  # this takes some time
                layout = device.get_result()

                y_page = []
                for lobj in layout:
                    if isinstance(lobj, LTTextBox):
                        x = lobj.bbox[0]
                        y = y_max + layout.height - lobj.bbox[3]  # y_max needed to keep pages in order, layout_height is needed becasuse bbox[3] is inverse y
                        text = lobj.get_text()

                        pdf_xloc.append(x)
                        pdf_yloc.append(y)
                        pdf_text.append(text)

                        y_page.append(y)

                y_max = max(y_page)

            paired_sorted = sorted(zip(pdf_yloc, pdf_xloc, pdf_text), key=lambda z: (z[0], z[1]))
            pdf_yloc, pdf_xloc, pdf_text = zip(*paired_sorted)
            text_raw = '\n'.join(pdf_text)

            text_cln = clean_text_records(text_raw)
            text_flt = filter_text_to_sections_selected(text_cln, sections=['Title', 'Introduction', 'Emissions data'])
            return text_flt
        except Exception as e:
            return None

    return None

parse_pdf_udf = udf(lambda z:parse_pdf("get", z), StringType())  

# COMMAND ----------

# MAGIC %md
# MAGIC ###### Main part: request url, parse pdf to text, clean text, log clean text to silver table

# COMMAND ----------

df_urls_small = df_urls.limit(50)
df_urls_small_extended = df_urls_small.withColumn("company_report_text", parse_pdf_udf(col("company_report_url")))

# COMMAND ----------


table_name = 'company_emission_report_text'
table_schema = "pkey INT, company_report_url STRING, company_report_text STRING"
spark.sql(
    f"""
        CREATE TABLE {table_name}
        ({table_schema})
        TBLPROPERTIES ("scraping_emissions_pipeline.quality" = "silver")
    """
)
df_urls_small_extended.write.insertInto(table_name)
